package isp;

public class Dog implements IAnimal, IWalkAnimal{
    @Override
    public void eat() {
        System.out.println("dog can eat");
    }

    @Override
    public void walk() {

        System.out.println("dog can walk");
    }

    /* @Override
    public void fly() {
        throw new UnsupportedOperationException();
    }*/
}
//