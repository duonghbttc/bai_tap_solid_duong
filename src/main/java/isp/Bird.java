package isp;

public class Bird implements IAnimal, IFlyAnimal, IWalkAnimal {

    @Override
    public void eat() { System.out.println("bird can eat");}

    @Override
    public void walk() {
        System.out.println("bird can walk");
    }

    @Override
    public void fly() {
        System.out.println("bird can fly");
    }
}
