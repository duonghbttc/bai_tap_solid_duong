package openclosed.ex0;

import single.responsibility.cohension.ex3.Database;


public class TagPost extends Post{

    public void TagPost(Database db, String postMessage){

        if(postMessage.startsWith("#")){
            db.addAsTag(postMessage);
        }
        if(postMessage.startsWith("mention")){
            db.addAsMentionPost(postMessage);
        }else {
            db.Add(postMessage);
        }
    }
}