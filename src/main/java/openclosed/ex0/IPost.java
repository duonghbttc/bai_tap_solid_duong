package openclosed.ex0;

import single.responsibility.cohension.ex3.Database;

public interface IPost {

    public void CreatePost(Database db, String postMessage);

    public void TagPost(Database db, String postMessage);

}
