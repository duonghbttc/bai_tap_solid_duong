package dry;

import java.util.ArrayList;
import java.util.List;
import java.util.jar.Attributes;

public class GoodParent {

    public List<Person> findAllGoodPeople(List<Person> persons){

        //tạo hàm tìm GoodPeople( tìm các People có đặc điểm phù hợp.
        List<Person> results = new ArrayList<>();
        for (Person person : persons){
            if (person.name.startsWith("T")
                    && person.age > 25
                    && person.hairColor == HairColor.BLACK
                    && person.skinColor == SkinColor.WHITE
            ) {
                results.add(person);
            }
        }
        return results;
    }

    public List<Person> findAllGoodMan(List<Person> persons) {
        /*sử dụng lại hàm findAllGoodPeople để lọc GoodPeople */
        List<Person> results = new ArrayList<>();
        List<Person> goodPeople = findAllGoodPeople(persons);
        for (Person person : goodPeople){
            if (person.sex == Gender.MALE){
                results.add(person);
            }
        }
        return results;
    }

    public List<Person> findAllGoodGirl(List<Person> persons) {
        List<Person> results = new ArrayList<>();
        List<Person> goodPeople = findAllGoodPeople(persons);
        for (Person person : goodPeople){
            if (person.sex == Gender.FEMALE){
                results.add(person);
            }
        }
        return results;
    }
}





