package dip;

import openclosed.ex0.Post;
import openclosed.ex0.IPost;
import openclosed.ex0.TagPost;
import single.responsibility.cohension.ex3.Database;

//public class OfficeUser {
//    public void publishNewPost() {
//        Database db = new Database();
//        String postMessage = "example message";
//        Post post = new Post();
//        post.CreatePost(db, postMessage);
//    }
//}


public class OfficeUser {

    private IPost post = new Post();

    public void publishNewPost(Database db, String postMessage){

         post.CreatePost(db, postMessage);
    }


}