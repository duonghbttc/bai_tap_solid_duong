package kiss;

public class IntegrationHandlerFactory  {

    public IntegrationHandler getHandler(IntegrationHandler integrations, String integration) {

        IntegrationHandler result;
        result = integrations.getHandlerFor(integration);
        return result;
    }

}
