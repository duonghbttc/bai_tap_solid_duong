package kiss;

import static kiss.Constant.SMS;

public class SMSIntegrationHandler implements IntegrationHandler {

    private final SMSIntegrationHandler smsHandler;

    public SMSIntegrationHandler(SMSIntegrationHandler smsHandler) {
        this.smsHandler = smsHandler;
    }

    @Override
    public IntegrationHandler getHandlerFor(String integration) {
        if (SMS.equals(integration)) {
            return smsHandler;
        }else{
            throw  new IllegalArgumentException("No handler found for integration: " + integration);
        }

    }
}
