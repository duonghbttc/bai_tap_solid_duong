package kiss;

import static kiss.Constant.PUSH;

public class PushIntegrationHandler implements IntegrationHandler{

    private final PushIntegrationHandler pushHandler;

    public PushIntegrationHandler(PushIntegrationHandler pushHandler) {
        this.pushHandler = pushHandler;
    }

    @Override
    public IntegrationHandler getHandlerFor(String integration){
        if (PUSH.equals(integration)) {
            return pushHandler;
        }else{
            throw  new IllegalArgumentException("No handler found for integration: " + integration);
        }
    }
}


