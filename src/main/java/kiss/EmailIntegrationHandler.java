package kiss;

import static kiss.Constant.EMAIL;

public class EmailIntegrationHandler implements IntegrationHandler {

    private final EmailIntegrationHandler emailHandler;

    public EmailIntegrationHandler(EmailIntegrationHandler emailHandler) {
        this.emailHandler = emailHandler;
    }

    @Override
    public IntegrationHandler getHandlerFor(String integration) {
        if (EMAIL.equals(integration)) {
            return emailHandler;
        }else{
            throw  new IllegalArgumentException("No handler found for integration: " + integration);
        }
    }
}
