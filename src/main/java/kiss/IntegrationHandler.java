package kiss;

public interface IntegrationHandler {

    public IntegrationHandler getHandlerFor(String integration);

}
