package single.responsibility.cohension.ex2;


public class Student {
    int id;
    String name;
    int age;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }


}

/* class student chỉ nên đảm nhận các hàm xử lý liên quan đến đối tưởng student, ko nên đảm nhận cả
   việc lưu dữ liệu vào database -> chuyển hàm SaveToDatabase sang 1 class thực hiện các công việc
   với database
 */