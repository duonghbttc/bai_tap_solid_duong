package single.responsibility.cohension.ex3;

public class Database {
    public void Add(String postMessage) {

    }

    //public void logError(String s, String toString) {

    //}

    public void addAsTag(String postMessage) {

    }

    public void addAsMentionPost(String postMessage) {

    }
}
/* class Database chỉ nên thực hiện các công việc thêm sửa xóa trên database, chứ ko nên làm cả
   việc thông báo lỗi  -> chuyển hàm logError sang 1 class chuyên nhiệm vụ log lỗi
 */
