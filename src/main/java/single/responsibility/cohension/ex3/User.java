package single.responsibility.cohension.ex3;

import java.io.File;

public class User {
    private PostError postError = new PostError();
    public void CreatePost(Database db, String postMessage) {
        try {
            db.Add(postMessage);
        } catch (Exception ex) {
            postError.logError("An error occured: ", ex.toString());
            FileUtil.writeAllText("LocalErrors.txt", ex.toString());
        }
    }
}
