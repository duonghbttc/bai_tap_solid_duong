package single.responsibility.cohension.ex1;

public class MoveSquare extends SquareCalculate{

    public void rotate () {
        System.out.printf("we rotate this square now!!");
    }

    public void draw() {
        System.out.println("draw square now!!");
    }

}